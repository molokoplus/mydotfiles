export ZSH=/home/nstefani/.oh-my-zsh

ZSH_THEME="robbyrussell"
DISABLE_AUTO_TITLE=true

#Plugins 
plugins=(git pip docker docker-compose rsync fzf python django aws kubectl zsh-autosuggestions zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
export LANG=es_AR.UTF-8

# Fuzzy finder config
export FZF_BASE=~/.fzf/bin/fzf

# PYENV setup
export PYENV_ROOT=~/.pyenv

# GO Setup
export GOPATH=$HOME/Projects/gocode

# PATH additions
PATH=$PYENV_ROOT/bin:~/.local/bin:~/.pyenv/bin:$GOPATH/bin:$PATH

#virtualenvwrapper init
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source ~/.local/bin/virtualenvwrapper.sh
export WORKON_HOME=$HOME/.Envs
export PROJECT_HOME=$HOME/Projects

# Aliases
eval "$(thefuck --alias)"
alias ls='ls --color=auto'
alias meteo="curl 'http://wttr.in/cordoba?lang=es'"
alias tmux='tmux -2'

#Call script for download subs 
alias download='~/Projects/scripts/download_sub.sh'

#Default Editor
export EDITOR="nvim"

# Inits
eval "$(pyenv init -)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#[ -z "$TMUX" ] && export TERM=xterm-256color && exec tmux

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
